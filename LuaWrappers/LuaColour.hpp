#ifndef LUACOLOUR_HPP
#define LUACOLOUR_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include <SFML/Graphics/Color.hpp>

int lColourConstructor(lua_State *L);
int lColourDestructor(lua_State *L);
sf::Color* lCheckColour(lua_State *L,int n);
int lColour_toInteger(lua_State *L);
int lColour_getRed(lua_State *L);
int lColour_getGreen(lua_State *L);
int lColour_getBlue(lua_State *L);
int lColour_getAlpha(lua_State *L);
int lColour_setRed(lua_State *L);
int lColour_setGreen(lua_State *L);
int lColour_setBlue(lua_State *L);
int lColour_setAlpha(lua_State *L);

int lColour_Black(lua_State *L);
int lColour_White(lua_State *L);
int lColour_Red(lua_State *L);
int lColour_Blue(lua_State *L);
int lColour_Yelow(lua_State *L);
int lColour_Magenta(lua_State *L);
int lColour_Cyan(lua_State *L);
int lColour_Transparent(lua_State *L);

int lColour_Add(lua_State *L);
int lColour_Negate(lua_State *L);
int lColour_Multiply(lua_State *L);
int lColour_Equal(lua_State *L);

void RegisterColour(lua_State *L);

#endif // LUACOLOUR_HPP
