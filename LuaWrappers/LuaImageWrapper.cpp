#include "LuaImageWrapper.hpp"
#include "LuaColour.hpp"

int lImageWrapperConstructor(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	/*
	 * Stack:
	 * -1 = string
	*/
	ImageWrapper** udata = (ImageWrapper **)lua_newuserdata(L, sizeof(ImageWrapper *));
	*udata = new ImageWrapper(name);
	/*
	 * Stack:
	 * -1 = userdata
	 * -2 = string
	*/
	luaL_getmetatable(L, "_image");
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = userdata
	 * -3 = string
	*/
	lua_setmetatable(L, -2);
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = string
	*/
	return 1;
}
int lImageWrapperDestructor(lua_State *L)
{
	delete lCheckImageWrapper(L,1);
	return 0;
}
ImageWrapper* lCheckImageWrapper(lua_State *L,int n)
{
	return *static_cast<ImageWrapper**>(luaL_checkudata(L, n, "_image") );
}
int lImage_getSizeR(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		sf::Vector2u tmp_vctr = tmp->getSize();
		lua_pushinteger(L,tmp_vctr.x);
		lua_pushinteger(L,tmp_vctr.y);
		return 2;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getSizeT(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		sf::Vector2u tmp_vctr = tmp->getSize();
		lua_newtable(L);
		lua_pushinteger(L,tmp_vctr.x);
		lua_setfield(L,-1,"x");
		lua_pushinteger(L,tmp_vctr.y);
		lua_setfield(L,-1,"y");
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_createMaskFromColor(lua_State *L)
{
	try {
		int args = lua_gettop(L);
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		sf::Color* tmp_clr = lCheckColour(L,2);
		if(args >= 3)
		{
			sf::Uint8 tmp_alph = luaL_checkinteger(L,3);
			tmp->createMaskFromColor(*tmp_clr,tmp_alph);
		}
		else tmp->createMaskFromColor(*tmp_clr);
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_setPixel(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color* tmp_clr = lCheckColour(L,4);
		tmp->setPixel(x,y,*tmp_clr);
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getPixelT(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_newtable(L);
		lua_pushinteger(L,clr.r);
		lua_setfield(L,-1,"r");
		lua_pushinteger(L,clr.g);
		lua_setfield(L,-1,"g");
		lua_pushinteger(L,clr.b);
		lua_setfield(L,-1,"b");
		lua_pushinteger(L,clr.a);
		lua_setfield(L,-1,"a");
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getPixelR(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_pushinteger(L,clr.r);
		lua_pushinteger(L,clr.g);
		lua_pushinteger(L,clr.b);
		lua_pushinteger(L,clr.a);
		return 4;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getRed(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_pushinteger(L,clr.r);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getBlue(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_pushinteger(L,clr.b);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getGreen(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_pushinteger(L,clr.g);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getAlpha(lua_State *L)
{
	try {
		ImageWrapper* tmp = lCheckImageWrapper(L,1);
		unsigned int x = luaL_checkinteger(L,2);
		unsigned int y = luaL_checkinteger(L,3);
		sf::Color clr = tmp->getPixel(x,y);
		lua_pushinteger(L,clr.a);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_flipHorizontally(lua_State *L)
{
	try {
		lCheckImageWrapper(L,1)->flipHorizontally();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_flipVertically(lua_State *L)
{
	try {
		lCheckImageWrapper(L,1)->flipVertically();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lImage_getPixelC(lua_State *L)
{
	ImageWrapper* temp = lCheckImageWrapper(L,1);
	unsigned int x = luaL_checkinteger(L,2);
	unsigned int y = luaL_checkinteger(L,3);
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(temp->getPixel(x,y));
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}

const luaL_Reg LuaImageFunctions[] = {
	{ "new", lImageWrapperConstructor },
	{ "__call", lImageWrapperConstructor },
	{ "__gc", lImageWrapperDestructor },
	{ "getSizeT", lImage_getSizeT} ,
	{ "getSizeR", lImage_getSizeR} ,
	{ "createMaskFromColor", lImage_createMaskFromColor} ,
	{ "setPixel", lImage_setPixel} ,
	{ "getPixelT", lImage_getPixelT } ,
	{ "getPixelR", lImage_getPixelR } ,
	{ "getPixelC", lImage_getPixelC } ,
	{ "getRed", lImage_getRed } ,
	{ "getGreen", lImage_getGreen } ,
	{ "getBlue", lImage_getBlue } ,
	{ "getAlpha", lImage_getAlpha } ,
	{ "flipHorizontally", lImage_flipHorizontally} ,
	{ "flipVertically", lImage_flipVertically} ,
	{ NULL, NULL }
};

void RegisterImageWrapper(lua_State *L)
{
	RegisterColour(L);
	luaL_newmetatable(L, "_image");
	luaL_register(L, NULL, LuaImageFunctions);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "image");
}
