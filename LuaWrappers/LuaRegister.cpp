#include "LuaRegister.hpp"
#include "LuaSound.hpp"
#include "LuaVector.hpp"
#include "LuaMusicWrapper.hpp"
#include "LuaImageWrapper.hpp"

void RegisterAll(lua_State *L)
{
	RegisterLuaFunctions(L);
	RegisterVector(L);
	RegisterSoundWrapper(L);
	RegisterMusicWrapper(L);
	RegisterImageWrapper(L);
}
