#include "LuaVector.hpp"
void RegisterVector(lua_State *L)
{
	RegisterVector2D(L);
	RegisterVector3D(L);
}
