#ifndef LUAREGISTER_HPP
#define LUAREGISTER_HPP
#include "PhysFsWrappers/PhysLua.hpp"

void RegisterAll(lua_State *L);

#endif // LUAREGISTER_HPP
