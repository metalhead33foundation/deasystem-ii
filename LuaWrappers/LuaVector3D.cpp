#include "LuaVector3D.hpp"

int lVector3dfConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	if(!args || args == 2) return luaL_error(L,"Invalid number of arguments.");
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	if(args == 1)
	{
		sf::Vector3f* tmp = lCheckVector3df(L,1);
		*udata = new sf::Vector3f(*tmp);
	}
	if(args >= 3)
	{
		float x = luaL_checknumber(L,1);
		float y = luaL_checknumber(L,2);
		float z = luaL_checknumber(L,3);
		*udata = new sf::Vector3f(x,y,z);
	}
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3dfDestructor(lua_State *L)
{
	delete lCheckVector3df(L,1);
	return 0;
}
sf::Vector3f* lCheckVector3df(lua_State *L,int n)
{
	return *static_cast<sf::Vector3f**>(luaL_checkudata(L, n, "_vector3df") );
}

int lVector3diConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	if(!args || args == 2) return luaL_error(L,"Invalid number of arguments.");
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	if(args == 1)
	{
		sf::Vector3i* tmp = lCheckVector3di(L,1);
		*udata = new sf::Vector3i(*tmp);
	}
	if(args >= 3)
	{
		int x = luaL_checkinteger(L,1);
		int y = luaL_checkinteger(L,2);
		int z = luaL_checkinteger(L,3);
		*udata = new sf::Vector3i(x,y,z);
	}
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3diDestructor(lua_State *L)
{
	delete lCheckVector3di(L,1);
	return 0;
}
sf::Vector3i* lCheckVector3di(lua_State *L,int n)
{
	return *static_cast<sf::Vector3i**>(luaL_checkudata(L, n, "_vector3di") );
}

int lVector3df_getX(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	lua_pushnumber(L,tmp->x);
	return 1;
}
int lVector3df_setX(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	tmp->x = luaL_checknumber(L,2);
	return 0;
}
int lVector3df_getY(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	lua_pushnumber(L,tmp->y);
	return 1;
}
int lVector3df_setY(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	tmp->y = luaL_checknumber(L,2);
	return 0;
}
int lVector3df_getZ(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	lua_pushnumber(L,tmp->z);
	return 1;
}
int lVector3df_setZ(lua_State *L)
{
	sf::Vector3f* tmp = lCheckVector3df(L,1);
	tmp->z = luaL_checknumber(L,2);
	return 0;
}
int lVector3di_getX(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	lua_pushinteger(L,tmp->x);
	return 1;
}
int lVector3di_setX(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	tmp->x = luaL_checkinteger(L,2);
	return 0;
}
int lVector3di_getY(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	lua_pushinteger(L,tmp->y);
	return 1;
}
int lVector3di_setY(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	tmp->y = luaL_checkinteger(L,2);
	return 0;
}
int lVector3di_getZ(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	lua_pushinteger(L,tmp->z);
	return 1;
}
int lVector3di_setZ(lua_State *L)
{
	sf::Vector3i* tmp = lCheckVector3di(L,1);
	tmp->z = luaL_checkinteger(L,2);
	return 0;
}

int lVector3df_add(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	sf::Vector3f* right = lCheckVector3df(L,2);
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	*udata = new sf::Vector3f(*left + *right);
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3df_sub(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	sf::Vector3f* right = lCheckVector3df(L,2);
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	*udata = new sf::Vector3f(*left - *right);
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3df_mul(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	float right = luaL_checknumber(L,2);
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	*udata = new sf::Vector3f(*left * right);
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3df_div(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	float right = luaL_checknumber(L,2);
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	*udata = new sf::Vector3f(*left / right);
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3df_unm(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
	*udata = new sf::Vector3f(-*left);
	luaL_getmetatable(L, "_vector3df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3df_eq(lua_State *L)
{
	sf::Vector3f* left = lCheckVector3df(L,1);
	sf::Vector3f* right = lCheckVector3df(L,2);
	lua_pushboolean(L,*left == *right);
	return 1;
}
int lVector3di_add(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	sf::Vector3i* right = lCheckVector3di(L,2);
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	*udata = new sf::Vector3i(*left + *right);
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3di_sub(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	sf::Vector3i* right = lCheckVector3di(L,2);
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	*udata = new sf::Vector3i(*left - *right);
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3di_mul(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	int right = luaL_checkinteger(L,2);
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	*udata = new sf::Vector3i(*left * right);
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3di_div(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	int right = luaL_checkinteger(L,2);
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	*udata = new sf::Vector3i(*left / right);
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3di_unm(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	sf::Vector3i** udata = (sf::Vector3i **)lua_newuserdata(L, sizeof(sf::Vector3i *));
	*udata = new sf::Vector3i(-*left);
	luaL_getmetatable(L, "_vector3di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector3di_eq(lua_State *L)
{
	sf::Vector3i* left = lCheckVector3di(L,1);
	sf::Vector3i* right = lCheckVector3di(L,2);
	lua_pushboolean(L,*left == *right);
	return 1;
}

int lVector3df_getTotal(lua_State *L)
{
	sf::Vector3f* temp = lCheckVector3df(L,1);
	lua_pushnumber(L,temp->x);
	lua_pushnumber(L,temp->y);
	lua_pushnumber(L,temp->z);
	return 3;
}
int lVector3di_getTotal(lua_State *L)
{
	sf::Vector3i* temp = lCheckVector3di(L,1);
	lua_pushinteger(L,temp->x);
	lua_pushinteger(L,temp->y);
	lua_pushinteger(L,temp->z);
	return 3;
}
int lVector3df_setTotal(lua_State *L)
{
	sf::Vector3f* temp = lCheckVector3df(L,1);
	temp->x = luaL_checknumber(L,2);
	temp->y = luaL_checknumber(L,3);
	temp->z = luaL_checknumber(L,4);
	return 0;
}
int lVector3di_setTotal(lua_State *L)
{
	sf::Vector3i* temp = lCheckVector3di(L,1);
	temp->x = luaL_checkinteger(L,2);
	temp->y = luaL_checkinteger(L,3);
	temp->z = luaL_checkinteger(L,4);
	return 0;
}

const luaL_Reg LuaVector3Float[] = {
	{ "new", lVector3dfConstructor},
	{ "__call", lVector3dfConstructor},
	{ "__gc", lVector3dfDestructor},
	{ "get", lVector3df_getTotal},
	{ "set", lVector3df_setTotal},
	{ "getX", lVector3df_getX},
	{ "setX", lVector3df_setX},
	{ "getY", lVector3df_getY},
	{ "setY", lVector3df_setY},
	{ "getZ", lVector3df_getZ},
	{ "setZ", lVector3df_setZ},
	{ "__add", lVector3df_add},
	{ "__sub", lVector3df_sub},
	{ "__mul", lVector3df_mul},
	{ "__div", lVector3df_div},
	{ "__unm", lVector3df_unm},
	{ "__eq", lVector3df_eq},
	{ NULL, NULL }
};

void RegisterVector3DFloat(lua_State *L)
{
	luaL_newmetatable(L, "_vector3df");
	luaL_register(L, NULL, LuaVector3Float);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "vector3df");
}

const luaL_Reg LuaVector3Int[] = {
	{ "new", lVector3diConstructor},
	{ "__call", lVector3diConstructor},
	{ "__gc", lVector3diDestructor},
	{ "get", lVector3di_getTotal},
	{ "set", lVector3di_setTotal},
	{ "getX", lVector3di_getX},
	{ "setX", lVector3di_setX},
	{ "getY", lVector3di_getY},
	{ "setY", lVector3di_setY},
	{ "getZ", lVector3di_getZ},
	{ "setZ", lVector3di_setZ},
	{ "__add", lVector3di_add},
	{ "__sub", lVector3di_sub},
	{ "__mul", lVector3di_mul},
	{ "__div", lVector3di_div},
	{ "__unm", lVector3di_unm},
	{ "__eq", lVector3di_eq},
	{ NULL, NULL }
};

void RegisterVector3DInt(lua_State *L)
{
	luaL_newmetatable(L, "_vector3di");
	luaL_register(L, NULL, LuaVector3Int);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "vector3di");
}
void RegisterVector3D(lua_State *L)
{
	RegisterVector3DInt(L);
	RegisterVector3DFloat(L);
}
