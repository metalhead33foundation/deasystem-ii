#include "LuaSound.hpp"
#include "LuaVector.hpp"
#include "LuaSoundBuffer.hpp"

int lSoundWrapperConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	sf::Sound** udata = (sf::Sound **)lua_newuserdata(L, sizeof(sf::Sound *));
	if(args)
	{
		SoundBufferPhysfs* buf = lCheckSoundBuffer(L,1);
		*udata = new sf::Sound(*buf->GetBuffer());
	}
	else
	{
		*udata = new sf::Sound();
	}
	luaL_getmetatable(L, "_sound");
	lua_setmetatable(L, -2);
	return 1;
}

int lSoundWrapperDestructor(lua_State *L)
{
	delete lCheckSoundWrapper(L,1);
	return 0;
}
sf::Sound* lCheckSoundWrapper(lua_State *L,int n)
{
	return *static_cast<sf::Sound**>(luaL_checkudata(L, n, "_sound") );
}

int lSound_setBuffer(lua_State *L)
{
	try {
		sf::Sound* temp = lCheckSoundWrapper(L,1);
		SoundBufferPhysfs* buf = lCheckSoundBuffer(L,2);
		temp->setBuffer(*buf->GetBuffer());
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_play(lua_State *L)
{
	try {
		lCheckSoundWrapper(L,1)->play();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_pause(lua_State *L)
{
	try {
		lCheckSoundWrapper(L,1)->pause();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_stop(lua_State *L)
{
	try {
		lCheckSoundWrapper(L,1)->stop();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_isPlaying(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckSoundWrapper(L,1)->getStatus() == sf::Sound::Playing);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_isPaused(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckSoundWrapper(L,1)->getStatus() == sf::Sound::Paused);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_isStopped(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckSoundWrapper(L,1)->getStatus() == sf::Sound::Stopped);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setLoop(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setLoop(lua_toboolean(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getLoop(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckSoundWrapper(L,1)->getLoop());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setPitch(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setPitch(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setVolume(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setVolume(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setPosition(lua_State *L)
{
	try {
		int args = lua_gettop(L);
		if(args >= 4) {
		lCheckSoundWrapper(L,1)->setPosition(lua_tonumber(L,2),
											 lua_tonumber(L,3),
											 lua_tonumber(L,4)
											 );
		}
		else
		{
			sf::Sound* temp = lCheckSoundWrapper(L,1);
			sf::Vector3f* vect = lCheckVector3df(L,2);
			temp->setPosition(*vect);
		}
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getPosition(lua_State *L)
{
	try {
		sf::Sound* wrap = lCheckSoundWrapper(L,1);
		sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
		*udata = new sf::Vector3f(wrap->getPosition() );
		luaL_getmetatable(L, "_vector3df");
		lua_setmetatable(L, -2);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setRelativeToListener(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setRelativeToListener(lua_toboolean(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setMinDistance(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setMinDistance(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_setAttenuation(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lCheckSoundWrapper(L,1)->setAttenuation(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getPitch(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckSoundWrapper(L,1)->getPitch());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getVolume(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckSoundWrapper(L,1)->getVolume());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_isRelativeToListener(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckSoundWrapper(L,1)->isRelativeToListener());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getMinDistance(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckSoundWrapper(L,1)->getMinDistance());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSound_getAttenuation(lua_State *L)
{
	try {
		//lCheckSoundWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckSoundWrapper(L,1)->getAttenuation());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}

const luaL_Reg LuaSoundFunctions[] = {
	{ "new", lSoundWrapperConstructor },
	{ "__call", lSoundWrapperConstructor },
	{ "__gc", lSoundWrapperDestructor },
	{ "setBuffer", lSound_setBuffer},
	{ "play", lSound_play},
	{ "pause", lSound_pause},
	{ "stop", lSound_stop},
	{ "isPlaying", lSound_isPlaying},
	{ "isPaused", lSound_isPaused},
	{ "isStopped", lSound_isStopped},
	{ "setLoop", lSound_setLoop},
	{ "getLoop", lSound_getLoop},
	{ "setPitch", lSound_setPitch},
	{ "setVolume", lSound_setVolume},
	{ "setPosition", lSound_setPosition},
	{ "setRelativeToListener", lSound_setRelativeToListener},
	{ "setMinDistance", lSound_setMinDistance},
	{ "setAttenuation", lSound_setAttenuation},
	{ "getPitch", lSound_getPitch},
	{ "getVolume", lSound_getVolume},
	{ "getPosition", lSound_getPosition},
	{ "isRelativeToListener", lSound_isRelativeToListener},
	{ "getMinDistance", lSound_getMinDistance},
	{ "getAttenuation", lSound_getAttenuation},
	{ NULL, NULL }
};

void RegisterSoundWrapper(lua_State *L)
{
	RegisterSoundBuffer(L);
	luaL_newmetatable(L, "_sound");
	luaL_register(L, NULL, LuaSoundFunctions);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "sound");
}
