#ifndef LUAIMAGEWRAPPER_HPP
#define LUAIMAGEWRAPPER_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include "PhysFsWrappers/ImageWrapper.hpp"

int lImageWrapperConstructor(lua_State *L);
int lImageWrapperDestructor(lua_State *L);
ImageWrapper* lCheckImageWrapper(lua_State *L,int n);
int lImage_getSizeT(lua_State *L);
int lImage_getSizeR(lua_State *L);
int lImage_createMaskFromColor(lua_State *L);
int lImage_setPixel(lua_State *L);
int lImage_getPixelT(lua_State *L);
int lImage_getPixelR(lua_State *L);
int lImage_getPixelC(lua_State *L);
int lImage_getRed(lua_State *L);
int lImage_getBlue(lua_State *L);
int lImage_getGreen(lua_State *L);
int lImage_getAlpha(lua_State *L);
int lImage_flipHorizontally(lua_State *L);
int lImage_flipVertically(lua_State *L);
void RegisterImageWrapper(lua_State *L);

#endif // LUAIMAGEWRAPPER_HPP
