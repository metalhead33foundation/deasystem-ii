#ifndef LUAVECTOR3D_HPP
#define LUAVECTOR3D_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include <SFML/System/Vector3.hpp>

int lVector3dfConstructor(lua_State *L);
int lVector3dfDestructor(lua_State *L);
sf::Vector3f* lCheckVector3df(lua_State *L,int n);

int lVector3df_getTotal(lua_State *L);
int lVector3df_setTotal(lua_State *L);
int lVector3df_getX(lua_State *L);
int lVector3df_setX(lua_State *L);
int lVector3df_getY(lua_State *L);
int lVector3df_setY(lua_State *L);
int lVector3df_getZ(lua_State *L);
int lVector3df_setZ(lua_State *L);

int lVector3df_add(lua_State *L);
int lVector3df_sub(lua_State *L);
int lVector3df_mul(lua_State *L);
int lVector3df_div(lua_State *L);
int lVector3df_unm(lua_State *L);
int lVector3df_eq(lua_State *L);

int lVector3diConstructor(lua_State *L);
int lVector3diDestructor(lua_State *L);
sf::Vector3i* lCheckVector3di(lua_State *L,int n);

int lVector3di_getTotal(lua_State *L);
int lVector3di_setTotal(lua_State *L);
int lVector3di_getX(lua_State *L);
int lVector3di_setX(lua_State *L);
int lVector3di_getY(lua_State *L);
int lVector3di_setY(lua_State *L);
int lVector3di_getZ(lua_State *L);
int lVector3di_setZ(lua_State *L);

int lVector3di_add(lua_State *L);
int lVector3di_sub(lua_State *L);
int lVector3di_mul(lua_State *L);
int lVector3di_div(lua_State *L);
int lVector3di_unm(lua_State *L);
int lVector3di_eq(lua_State *L);

void RegisterVector3DFloat(lua_State *L);
void RegisterVector3DInt(lua_State *L);
void RegisterVector3D(lua_State *L);

#endif // LUAVECTOR3D_HPP
