#include "LuaSoundBuffer.hpp"

int lSoundBufferConstructor(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	/*
	 * Stack:
	 * -1 = string
	*/
	SoundBufferPhysfs** udata = (SoundBufferPhysfs **)lua_newuserdata(L, sizeof(SoundBufferPhysfs *));
	*udata = new SoundBufferPhysfs(name);
	/*
	 * Stack:
	 * -1 = userdata
	 * -2 = string
	*/
	luaL_getmetatable(L, "_soundbuffer");
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = userdata
	 * -3 = string
	*/
	lua_setmetatable(L, -2);
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = string
	*/
	return 1;
}
int lSoundBufferDestructor(lua_State *L)
{
	delete lCheckSoundBuffer(L,1);
	return 0;
}
SoundBufferPhysfs* lCheckSoundBuffer(lua_State *L,int index)
{
	return *static_cast<SoundBufferPhysfs**>(luaL_checkudata(L, index, "_soundbuffer") );
}
int lSoundBuffer_getSampleCount(lua_State *L)
{
	try {
	SoundBufferPhysfs* tmp = lCheckSoundBuffer(L,1);
	lua_pushinteger(L,tmp->getSampleCount());
	return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSoundBuffer_getSampleRate(lua_State *L)
{
	try {
	SoundBufferPhysfs* tmp = lCheckSoundBuffer(L,1);
	lua_pushinteger(L,tmp->getSampleRate());
	return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSoundBuffer_getChannelCount(lua_State *L)
{
	try {
	SoundBufferPhysfs* tmp = lCheckSoundBuffer(L,1);
	lua_pushinteger(L,tmp->getChannelCount());
	return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lSoundBuffer_getDuration(lua_State *L)
{
	try {
	SoundBufferPhysfs* tmp = lCheckSoundBuffer(L,1);
	lua_pushnumber(L,tmp->getDuration().asSeconds());
	return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}

const luaL_Reg LuaSbufferRegister[] = {
	{ "new", lSoundBufferConstructor },
	{ "__call", lSoundBufferConstructor },
	{ "__gc", lSoundBufferDestructor },

	{ "getSampleCount", lSoundBuffer_getSampleCount },
	{ "getSampleRate", lSoundBuffer_getSampleRate },
	{ "getChannelCount", lSoundBuffer_getChannelCount },
	{ "getDuration", lSoundBuffer_getDuration },

	{ NULL, NULL }
};

void RegisterSoundBuffer(lua_State *L) // the sound register will register it
{
	luaL_newmetatable(L, "_soundbuffer");
	luaL_register(L, NULL, LuaSbufferRegister);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "soundbuffer");
}
