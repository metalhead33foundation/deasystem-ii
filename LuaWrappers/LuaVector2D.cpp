#include "LuaVector2D.hpp"

int lVector2dfConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	if(!args) return luaL_error(L,"Invalid number of arguments.");
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	if(args == 1)
	{
		sf::Vector2f* tmp = lCheckVector2df(L,1);
		*udata = new sf::Vector2f(*tmp);
	}
	if(args >= 2)
	{
		float x = luaL_checknumber(L,1);
		float y = luaL_checknumber(L,2);
		*udata = new sf::Vector2f(x,y);
	}
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2dfDestructor(lua_State *L)
{
	delete lCheckVector2df(L,1);
	return 0;
}
sf::Vector2f* lCheckVector2df(lua_State *L,int n)
{
	return *static_cast<sf::Vector2f**>(luaL_checkudata(L, n, "_vector2df") );
}

int lVector2diConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	if(!args) return luaL_error(L,"Invalid number of arguments.");
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	if(args == 1)
	{
		sf::Vector2i* tmp = lCheckVector2di(L,1);
		*udata = new sf::Vector2i(*tmp);
	}
	if(args >= 2)
	{
		int x = luaL_checkinteger(L,1);
		int y = luaL_checkinteger(L,2);
		*udata = new sf::Vector2i(x,y);
	}
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2diDestructor(lua_State *L)
{
	delete lCheckVector2di(L,1);
	return 0;
}
sf::Vector2i* lCheckVector2di(lua_State *L,int n)
{
	return *static_cast<sf::Vector2i**>(luaL_checkudata(L, n, "_vector2di") );
}

int lVector2df_getX(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	lua_pushnumber(L,tmp->x);
	return 1;
}
int lVector2df_setX(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	tmp->x = luaL_checknumber(L,2);
	return 0;
}
int lVector2df_getY(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	lua_pushnumber(L,tmp->y);
	return 1;
}
int lVector2df_setY(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	tmp->y = luaL_checknumber(L,2);
	return 0;
}
int lVector2di_getX(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	lua_pushinteger(L,tmp->x);
	return 1;
}
int lVector2di_setX(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	tmp->x = luaL_checkinteger(L,2);
	return 0;
}
int lVector2di_getY(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	lua_pushinteger(L,tmp->y);
	return 1;
}
int lVector2di_setY(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	tmp->y = luaL_checkinteger(L,2);
	return 0;
}

int lVector2df_add(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	sf::Vector2f* right = lCheckVector2df(L,2);
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	*udata = new sf::Vector2f(*left + *right);
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2df_sub(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	sf::Vector2f* right = lCheckVector2df(L,2);
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	*udata = new sf::Vector2f(*left - *right);
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2df_mul(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	float right = luaL_checknumber(L,2);
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	*udata = new sf::Vector2f(*left * right);
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2df_div(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	float right = luaL_checknumber(L,2);
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	*udata = new sf::Vector2f(*left / right);
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2df_unm(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	sf::Vector2f** udata = (sf::Vector2f **)lua_newuserdata(L, sizeof(sf::Vector2f *));
	*udata = new sf::Vector2f(-*left);
	luaL_getmetatable(L, "_vector2df");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2df_eq(lua_State *L)
{
	sf::Vector2f* left = lCheckVector2df(L,1);
	sf::Vector2f* right = lCheckVector2df(L,2);
	lua_pushboolean(L,*left == *right);
	return 1;
}
int lVector2di_add(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	sf::Vector2i* right = lCheckVector2di(L,2);
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	*udata = new sf::Vector2i(*left + *right);
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2di_sub(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	sf::Vector2i* right = lCheckVector2di(L,2);
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	*udata = new sf::Vector2i(*left - *right);
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2di_mul(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	int right = luaL_checkinteger(L,2);
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	*udata = new sf::Vector2i(*left * right);
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2di_div(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	int right = luaL_checkinteger(L,2);
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	*udata = new sf::Vector2i(*left / right);
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2di_unm(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	sf::Vector2i** udata = (sf::Vector2i **)lua_newuserdata(L, sizeof(sf::Vector2i *));
	*udata = new sf::Vector2i(-*left);
	luaL_getmetatable(L, "_vector2di");
	lua_setmetatable(L, -2);
	return 1;
}
int lVector2di_eq(lua_State *L)
{
	sf::Vector2i* left = lCheckVector2di(L,1);
	sf::Vector2i* right = lCheckVector2di(L,2);
	lua_pushboolean(L,*left == *right);
	return 1;
}

int lVector2df_getTotal(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	lua_pushnumber(L,tmp->x);
	lua_pushnumber(L,tmp->y);
	return 2;
}
int lVector2di_getTotal(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	lua_pushinteger(L,tmp->x);
	lua_pushinteger(L,tmp->y);
	return 2;
}
int lVector2df_setTotal(lua_State *L)
{
	sf::Vector2f* tmp = lCheckVector2df(L,1);
	tmp->x = luaL_checknumber(L,2);
	tmp->y = luaL_checknumber(L,3);
	return 0;
}
int lVector2di_setTotal(lua_State *L)
{
	sf::Vector2i* tmp = lCheckVector2di(L,1);
	tmp->x = luaL_checkinteger(L,2);
	tmp->y = luaL_checkinteger(L,3);
	return 0;
}

const luaL_Reg LuaVector2Float[] = {
	{ "new", lVector2dfConstructor},
	{ "__call", lVector2dfConstructor},
	{ "__gc", lVector2dfDestructor},
	{ "get", lVector2df_getTotal},
	{ "set", lVector2df_setTotal},
	{ "getX", lVector2df_getX},
	{ "setX", lVector2df_setX},
	{ "getY", lVector2df_getY},
	{ "setY", lVector2df_setY},
	{ "__add", lVector2df_add},
	{ "__sub", lVector2df_sub},
	{ "__mul", lVector2df_mul},
	{ "__div", lVector2df_div},
	{ "__unm", lVector2df_unm},
	{ "__eq", lVector2df_eq},
	{ NULL, NULL }
};

void RegisterVector2DFloat(lua_State *L)
{
	luaL_newmetatable(L, "_vector2df");
	luaL_register(L, NULL, LuaVector2Float);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "vector2df");
}

const luaL_Reg LuaVector2Int[] = {
	{ "new", lVector2diConstructor},
	{ "__call", lVector2diConstructor},
	{ "__gc", lVector2diDestructor},
	{ "get", lVector2di_getTotal},
	{ "set", lVector2di_setTotal},
	{ "getX", lVector2di_getX},
	{ "setX", lVector2di_setX},
	{ "getY", lVector2di_getY},
	{ "setY", lVector2di_setY},
	{ "__add", lVector2di_add},
	{ "__sub", lVector2di_sub},
	{ "__mul", lVector2di_mul},
	{ "__div", lVector2di_div},
	{ "__unm", lVector2di_unm},
	{ "__eq", lVector2di_eq},
	{ NULL, NULL }
};

void RegisterVector2DInt(lua_State *L)
{
	luaL_newmetatable(L, "_vector2di");
	luaL_register(L, NULL, LuaVector2Int);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "vector2di");
}
void RegisterVector2D(lua_State *L)
{
	RegisterVector2DInt(L);
	RegisterVector2DFloat(L);
}
