#include "LuaMusicWrapper.hpp"
#include "LuaVector.hpp"
#include <exception>


int lMusicWrapperConstructor(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	/*
	 * Stack:
	 * -1 = string
	*/
	MusicWrapper** udata = (MusicWrapper **)lua_newuserdata(L, sizeof(MusicWrapper *));
	*udata = new MusicWrapper(name);
	/*
	 * Stack:
	 * -1 = userdata
	 * -2 = string
	*/
	luaL_getmetatable(L, "_music");
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = userdata
	 * -3 = string
	*/
	lua_setmetatable(L, -2);
	/*
	 * Stack:
	 * -1 = music metatable
	 * -2 = string
	*/
	return 1;
}

int lMusicWrapperDestructor(lua_State *L)
{
	delete lCheckMusicWrapper(L,1);
	return 0;
}

MusicWrapper* lCheckMusicWrapper(lua_State *L,int n)
{
	return *static_cast<MusicWrapper**>(luaL_checkudata(L, n, "_music") );
}
int lMusic_getDuration(lua_State *L)
{
	try {
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getDuration());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_play(lua_State *L)
{
	try {
		lCheckMusicWrapper(L,1)->play();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_pause(lua_State *L)
{
	try {
		lCheckMusicWrapper(L,1)->pause();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_stop(lua_State *L)
{
	try {
		lCheckMusicWrapper(L,1)->stop();
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getChannelCount(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushinteger(L,lCheckMusicWrapper(L,1)->getChannelCount());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getSampleRate(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushinteger(L,lCheckMusicWrapper(L,1)->getSampleRate());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_isPlaying(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckMusicWrapper(L,1)->isPlaying());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_isPaused(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckMusicWrapper(L,1)->isPaused());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_isStopped(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckMusicWrapper(L,1)->isStopped());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setPlayingOffset(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setPlayingOffset(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getPlayingOffset(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getPlayingOffset());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setLoop(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setLoop(lua_toboolean(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getLoop(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckMusicWrapper(L,1)->getLoop());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setPitch(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setPitch(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setVolume(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setVolume(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setPosition(lua_State *L)
{
	try {
		int args = lua_gettop(L);
		if(args >= 4) {
		lCheckMusicWrapper(L,1)->setPosition(lua_tonumber(L,2),
											 lua_tonumber(L,3),
											 lua_tonumber(L,4)
											 );
		}
		else
		{
			MusicWrapper* temp = lCheckMusicWrapper(L,1);
			sf::Vector3f* vect = lCheckVector3df(L,2);
			temp->setPosition(*vect);
		}
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setX(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setX(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setY(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setY(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setZ(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setZ(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setRelativeToListener(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setRelativeToListener(lua_toboolean(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setMinDistance(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setMinDistance(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_setAttenuation(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lCheckMusicWrapper(L,1)->setAttenuation(lua_tonumber(L,2));
		return 0;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getPitch(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getPitch());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getVolume(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getVolume());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getPosition(lua_State *L)
{
	try {
		MusicWrapper* wrap = lCheckMusicWrapper(L,1);
		sf::Vector3f** udata = (sf::Vector3f **)lua_newuserdata(L, sizeof(sf::Vector3f *));
		*udata = new sf::Vector3f(wrap->getPosition() );
		luaL_getmetatable(L, "_vector3df");
		lua_setmetatable(L, -2);
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getX(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getX());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getY(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getY());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getZ(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getZ());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_isRelativeToListener(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushboolean(L,lCheckMusicWrapper(L,1)->isRelativeToListener());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getMinDistance(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getMinDistance());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}
int lMusic_getAttenuation(lua_State *L)
{
	try {
		//lCheckMusicWrapper(L,1)->stop();
		lua_pushnumber(L,lCheckMusicWrapper(L,1)->getAttenuation());
		return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what());
	}
}


const luaL_Reg LuaMusicFunctions[] = {
	{ "new", lMusicWrapperConstructor },
	{ "__call", lMusicWrapperConstructor },
	{ "__gc", lMusicWrapperDestructor },
	{ "getDuration", lMusic_getDuration },
	{ "play", lMusic_play },
	{ "pause", lMusic_pause },
	{ "stop", lMusic_stop },
	{ "getChannelCount", lMusic_getChannelCount },
	{ "getSampleRate", lMusic_getSampleRate },
	{ "isPlaying", lMusic_isPlaying },
	{ "isPaused", lMusic_isPaused },
	{ "isStopped", lMusic_isStopped },
	{ "setPlayingOffset", lMusic_setPlayingOffset },
	{ "getPlayingOffset", lMusic_getPlayingOffset },
	{ "setLoop", lMusic_setLoop },
	{ "getLoop", lMusic_getLoop },
	{ "setPitch", lMusic_setPitch },
	{ "setVolume", lMusic_setVolume },
	{ "setPosition", lMusic_setPosition },
	{ "setX", lMusic_setX },
	{ "setY", lMusic_setY },
	{ "setZ", lMusic_setZ },
	{ "setRelativeToListener", lMusic_setRelativeToListener },
	{ "setMinDistance", lMusic_setMinDistance },
	{ "setAttenuation", lMusic_setAttenuation },
	{ "getPitch", lMusic_getPitch },
	{ "getVolume", lMusic_getVolume },
	{ "getPosition", lMusic_getPosition },
	{ "getX", lMusic_getX },
	{ "getY", lMusic_getY },
	{ "getZ", lMusic_getZ },
	{ "isRelativeToListener", lMusic_isRelativeToListener },
	{ "getMinDistance", lMusic_getMinDistance },
	{ "getAttenuation", lMusic_getAttenuation },
	{ NULL, NULL }
};

void RegisterMusicWrapper(lua_State *L)
{
	luaL_newmetatable(L, "_music");
	luaL_register(L, NULL, LuaMusicFunctions);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "music");
}
