#ifndef LUASOUNDBUFFER_HPP
#define LUASOUNDBUFFER_HPP
#include "PhysFsWrappers/SoundBufferPhysfs.hpp"
#include "PhysFsWrappers/PhysLua.hpp"

// int lColourConstructor(lua_State *L);

int lSoundBufferConstructor(lua_State *L);
int lSoundBufferDestructor(lua_State *L);
SoundBufferPhysfs* lCheckSoundBuffer(lua_State *L,int index);
int lSoundBuffer_getSampleCount(lua_State *L);
int lSoundBuffer_getSampleRate(lua_State *L);
int lSoundBuffer_getChannelCount(lua_State *L);
int lSoundBuffer_getDuration(lua_State *L);

void RegisterSoundBuffer(lua_State *L);

#endif // LUASOUNDBUFFER_HPP
