#include "LuaColour.hpp"

int lColourConstructor(lua_State *L)
{
	int args = lua_gettop(L);
	if(!args || args == 2) return luaL_error(L,"Invalid number of arguments.");
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	if(args == 1)
	{
		sf::Uint32 tmp = luaL_checkinteger(L,1);
		*udata = new sf::Color(tmp);
	}
	if(args == 3)
	{
		sf::Uint8 r = luaL_checkinteger(L,1);
		sf::Uint8 g = luaL_checkinteger(L,2);
		sf::Uint8 b = luaL_checkinteger(L,3);
		*udata = new sf::Color(r,g,b);
	}
	if(args >= 4)
	{
		sf::Uint8 r = luaL_checkinteger(L,1);
		sf::Uint8 g = luaL_checkinteger(L,2);
		sf::Uint8 b = luaL_checkinteger(L,3);
		sf::Uint8 a = luaL_checkinteger(L,4);
		*udata = new sf::Color(r,g,b,a);
	}
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColourDestructor(lua_State *L)
{
	delete lCheckColour(L,1);
	return 0;
}
sf::Color* lCheckColour(lua_State *L,int n)
{
	return *static_cast<sf::Color**>(luaL_checkudata(L, n, "_colour") );
}
int lColour_toInteger(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	lua_pushinteger(L,temp->toInteger() );
	return 1;
}
int lColour_getRed(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	lua_pushinteger(L,temp->r);
	return 1;
}
int lColour_getGreen(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	lua_pushinteger(L,temp->g);
	return 1;
}
int lColour_getBlue(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	lua_pushinteger(L,temp->b);
	return 1;
}
int lColour_getAlpha(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	lua_pushinteger(L,temp->a);
	return 1;
}
int lColour_setRed(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	temp->r = luaL_checkinteger(L,2);
	return 0;
}
int lColour_setGreen(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	temp->g = luaL_checkinteger(L,2);
	return 0;
}
int lColour_setBlue(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	temp->b = luaL_checkinteger(L,2);
	return 0;
}
int lColour_setAlpha(lua_State *L)
{
	sf::Color* temp = lCheckColour(L,1);
	temp->a = luaL_checkinteger(L,2);
	return 0;
}

int lColour_Black(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Black);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_White(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::White);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Red(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Red);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Blue(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Blue);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Yelow(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Yellow);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Magenta(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Magenta);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Cyan(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Cyan);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Transparent(lua_State *L)
{
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(sf::Color::Transparent);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Add(lua_State *L)
{
	sf::Color* left = lCheckColour(L,1);
	sf::Color* right = lCheckColour(L,2);
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(*left + *right);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Negate(lua_State *L)
{
	sf::Color* left = lCheckColour(L,1);
	sf::Color* right = lCheckColour(L,2);
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(*left - *right);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Multiply(lua_State *L)
{
	sf::Color* left = lCheckColour(L,1);
	sf::Color* right = lCheckColour(L,2);
	sf::Color** udata = (sf::Color **)lua_newuserdata(L, sizeof(sf::Color *));
	*udata = new sf::Color(*left * *right);
	luaL_getmetatable(L, "_colour");
	lua_setmetatable(L, -2);
	return 1;
}
int lColour_Equal(lua_State *L)
{
	sf::Color* left = lCheckColour(L,1);
	sf::Color* right = lCheckColour(L,2);
	lua_pushboolean(L,*left == *right);
	return 1;
}

const luaL_Reg LuaColourFunctions[] = {
	{ "new", lColourConstructor },
	{ "__call", lColourConstructor },
	{ "__gc", lColourDestructor },
	{ "__add", lColour_Add },
	{ "__sub", lColour_Negate },
	{ "__mul", lColour_Multiply },
	{ "__eq", lColour_Equal },


	{ "toInteger", lColour_toInteger},
	{ "getRed", lColour_getRed},
	{ "getGreen", lColour_getGreen},
	{ "getBlue", lColour_getBlue},
	{ "getAlpha", lColour_getAlpha},
	{ "setRed", lColour_setRed},
	{ "setGreen", lColour_setGreen},
	{ "setBlue", lColour_setBlue},
	{ "setAlpha", lColour_setAlpha},

	{ "Black", lColour_Black},
	{ "White", lColour_White},
	{ "Red", lColour_Red},
	{ "Blue", lColour_Blue},
	{ "Yelow", lColour_Yelow},
	{ "Magenta", lColour_Magenta},
	{ "Cyan", lColour_Cyan},
	{ "Transparent", lColour_Transparent},

	{ NULL, NULL }
};

void RegisterColour(lua_State *L) // No need to call it separately, the IMageWrapper registrator will call it
{
	luaL_newmetatable(L, "_colour");
	luaL_register(L, NULL, LuaColourFunctions);
	lua_pushvalue(L, -1);
	lua_setfield(L, -1, "__index");
	lua_setglobal(L, "colour");
}
