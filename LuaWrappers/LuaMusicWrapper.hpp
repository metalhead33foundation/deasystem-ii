#ifndef LUAMUSICWRAPPER_HPP
#define LUAMUSICWRAPPER_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include "PhysFsWrappers/MusicWrapper.hpp"

int lMusicWrapperConstructor(lua_State *L);
int lMusicWrapperDestructor(lua_State *L);
MusicWrapper* lCheckMusicWrapper(lua_State *L,int n);
int lMusic_getDuration(lua_State *L);
int lMusic_play(lua_State *L);
int lMusic_pause(lua_State *L);
int lMusic_stop(lua_State *L);
int lMusic_getChannelCount(lua_State *L);
int lMusic_getSampleRate(lua_State *L);
int lMusic_isPlaying(lua_State *L);
int lMusic_isPaused(lua_State *L);
int lMusic_isStopped(lua_State *L);
int lMusic_setPlayingOffset(lua_State *L);
int lMusic_getPlayingOffset(lua_State *L);
int lMusic_setLoop(lua_State *L);
int lMusic_getLoop(lua_State *L);
int lMusic_setPitch(lua_State *L);
int lMusic_setVolume(lua_State *L);
int lMusic_setPosition(lua_State *L);
int lMusic_setX(lua_State *L);
int lMusic_setY(lua_State *L);
int lMusic_setZ(lua_State *L);
int lMusic_setRelativeToListener(lua_State *L);
int lMusic_setMinDistance(lua_State *L);
int lMusic_setAttenuation(lua_State *L);
int lMusic_getPitch(lua_State *L);
int lMusic_getVolume(lua_State *L);
int lMusic_getPosition(lua_State *L);
int lMusic_getX(lua_State *L);
int lMusic_getY(lua_State *L);
int lMusic_getZ(lua_State *L);
int lMusic_isRelativeToListener(lua_State *L);
int lMusic_getMinDistance(lua_State *L);
int lMusic_getAttenuation(lua_State *L);

void RegisterMusicWrapper(lua_State *L);

#endif // LUAMUSICWRAPPER_HPP
