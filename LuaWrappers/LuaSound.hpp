#ifndef LUASOUND_HPP
#define LUASOUND_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include <SFML/Audio/Sound.hpp>

int lSoundWrapperConstructor(lua_State *L);
int lSoundWrapperDestructor(lua_State *L);
sf::Sound* lCheckSoundWrapper(lua_State *L,int n);
int lSound_setBuffer(lua_State *L);
int lSound_play(lua_State *L);
int lSound_pause(lua_State *L);
int lSound_stop(lua_State *L);
int lSound_isPlaying(lua_State *L);
int lSound_isPaused(lua_State *L);
int lSound_isStopped(lua_State *L);
int lSound_setLoop(lua_State *L);
int lSound_getLoop(lua_State *L);
int lSound_setPitch(lua_State *L);
int lSound_setVolume(lua_State *L);
int lSound_setPosition(lua_State *L);
int lSound_setRelativeToListener(lua_State *L);
int lSound_setMinDistance(lua_State *L);
int lSound_setAttenuation(lua_State *L);
int lSound_getPitch(lua_State *L);
int lSound_getVolume(lua_State *L);
int lSound_getPosition(lua_State *L);
int lSound_isRelativeToListener(lua_State *L);
int lSound_getMinDistance(lua_State *L);
int lSound_getAttenuation(lua_State *L);

void RegisterSoundWrapper(lua_State *L);

#endif // LUASOUND_HPP
