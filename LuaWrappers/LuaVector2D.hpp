#ifndef LUAVECTOR2D_HPP
#define LUAVECTOR2D_HPP
#include "PhysFsWrappers/PhysLua.hpp"
#include <SFML/System/Vector2.hpp>

int lVector2dfConstructor(lua_State *L);
int lVector2dfDestructor(lua_State *L);
sf::Vector2f* lCheckVector2df(lua_State *L,int n);

int lVector2df_getTotal(lua_State *L);
int lVector2df_setTotal(lua_State *L);
int lVector2df_getX(lua_State *L);
int lVector2df_setX(lua_State *L);
int lVector2df_getY(lua_State *L);
int lVector2df_setY(lua_State *L);

int lVector2df_add(lua_State *L);
int lVector2df_sub(lua_State *L);
int lVector2df_mul(lua_State *L);
int lVector2df_div(lua_State *L);
int lVector2df_unm(lua_State *L);
int lVector2df_eq(lua_State *L);

int lVector2diConstructor(lua_State *L);
int lVector2diDestructor(lua_State *L);
sf::Vector2i* lCheckVector2di(lua_State *L,int n);

int lVector2di_getTotal(lua_State *L);
int lVector2di_setTotal(lua_State *L);
int lVector2di_getX(lua_State *L);
int lVector2di_setX(lua_State *L);
int lVector2di_getY(lua_State *L);
int lVector2di_setY(lua_State *L);

int lVector2di_add(lua_State *L);
int lVector2di_sub(lua_State *L);
int lVector2di_mul(lua_State *L);
int lVector2di_div(lua_State *L);
int lVector2di_unm(lua_State *L);
int lVector2di_eq(lua_State *L);

void RegisterVector2DFloat(lua_State *L);
void RegisterVector2DInt(lua_State *L);
void RegisterVector2D(lua_State *L);

#endif // LUAVECTOR2D_HPP
