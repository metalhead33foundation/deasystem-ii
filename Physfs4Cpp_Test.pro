QT += core
QT -= gui

CONFIG += c++11

TARGET = Physfs4Cpp_Test
CONFIG += console
CONFIG -= app_bundle
LIBS += -L"/usr/lib/" -L"/usr/local/lib"
include(qmake_modules/findPhysFS.pro)
include(qmake_modules/findSFML.pro)
include(qmake_modules/findLuaJIT.pro)
# include(qmake_modules/findGLEW.pro)
# include(qmake_modules/findGLFW.pro)
# include(qmake_modules/findOpenGL.pro)
# include(qmake_modules/findVulkan.pro)
include(qmake_modules/findAssimp.pro)

TEMPLATE = app

SOURCES += main.cpp \
    PhysFsWrappers/PhAssimp.cpp \
    PhysFsWrappers/PhySFML.cpp \
    PhysFsWrappers/MusicWrapper.cpp \
    PhysFsWrappers/Physfs4Cpp.cpp \
    PhysFsWrappers/PhysLua.cpp \
    LuaWrappers/LuaMusicWrapper.cpp \
    PhysFsWrappers/ImageWrapper.cpp \
    LuaWrappers/LuaImageWrapper.cpp \
    LuaWrappers/LuaColour.cpp \
    PhysFsWrappers/SoundBufferPhysfs.cpp \
    LuaWrappers/LuaSoundBuffer.cpp \
    LuaWrappers/LuaSound.cpp \
    LuaWrappers/LuaVector3D.cpp \
    LuaWrappers/LuaVector2D.cpp \
    LuaWrappers/LuaVector.cpp \
    LuaWrappers/LuaRegister.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    PhysFsWrappers/PhysLua.hpp \
    PhysFsWrappers/PhySFML.hpp \
    PhysFsWrappers/Physfs4Cpp.hpp \
    PhysFsWrappers/PhAssimp.hpp \
    PhysFsWrappers/MusicWrapper.hpp \
    LuaWrappers/LuaMusicWrapper.hpp \
    PhysFsWrappers/ImageWrapper.hpp \
    LuaWrappers/LuaImageWrapper.hpp \
    LuaWrappers/LuaColour.hpp \
    PhysFsWrappers/SoundBufferPhysfs.hpp \
    LuaWrappers/LuaSoundBuffer.hpp \
    LuaWrappers/LuaSound.hpp \
    LuaWrappers/LuaVector3D.hpp \
    LuaWrappers/LuaVector2D.hpp \
    LuaWrappers/LuaVector.hpp \
    LuaWrappers/LuaRegister.hpp
