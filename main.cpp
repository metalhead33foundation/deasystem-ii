#include <QCoreApplication>
#include "PhysFsWrappers/PhySFML.hpp"
#include "PhysFsWrappers/MusicWrapper.hpp"
#include "PhysFsWrappers/PhAssimp.hpp"
#include "PhysFsWrappers/PhysLua.hpp"
#include "LuaWrappers/LuaRegister.hpp"
#include <iostream>
#include <fstream>
#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	lua_State* L = luaL_newstate();
	luaL_openlibs(L);
	RegisterAll(L);
	PhysFs::FileHandle::Initialize(argv[0]);
	std::string line;
	std::ifstream infile("mountlist.txt");
	while (std::getline(infile, line))
	{
		PhysFs::FileHandle::Mount(line);
	}
	PhysFs::FileHandle::Mount(PhysFs::FileHandle::GetBaseDir() + PhysFs::FileHandle::GetDirSeparator() + "data" );
	int erred = phys_DoFileL(L,"main.lua");
	if(erred)
	{
		std::cout << "Lua error: " << luaL_checkstring(L, -1) << std::endl;
	}
	lua_close(L);
	PhysFs::FileHandle::Deinitialize();
	return 0;
	//return a.exec();
}
