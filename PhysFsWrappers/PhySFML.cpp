#include "PhySFML.hpp"

PhySFML::PhySFML(std::string name)
	: PhysFs::FileHandle(name,PhysFs::FileHandle::Mode::READ)
{
	;
}

sf::Int64 PhySFML::read(void* data, sf::Int64 size)
{
	return pRead(data,1,size);
}
sf::Int64 PhySFML::seek(sf::Int64 position)
{
	pSeek(position);
	return position;
}
sf::Int64 PhySFML::tell()
{
	return pTell();
}
sf::Int64 PhySFML::getSize()
{
	return pFileLength();
}
