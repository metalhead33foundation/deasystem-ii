#include "MusicWrapper.hpp"

void MusicWrapper::UpdatePosition()
{
	m_player->setPosition(x,y,z);
}
MusicWrapper::MusicWrapper(std::string filename)
{
	m_stream = new PhySFML(filename);
	m_player = new sf::Music();
	m_player->openFromStream(*m_stream);
	x = 0.00;
	y = 0.00;
	z = 0.00;
}
MusicWrapper::MusicWrapper(const char* filename)
{
	m_stream = new PhySFML(filename);
	m_player = new sf::Music();
	m_player->openFromStream(*m_stream);
	x = 0.00;
	y = 0.00;
	z = 0.00;
}
MusicWrapper::~MusicWrapper()
{
	if(m_player->getStatus() != sf::Music::Stopped) m_player->stop();
	delete m_player;
	delete m_stream;
}
sf::Music* MusicWrapper::GetPlayer()
{
	return m_player;
}
void MusicWrapper::CommitSuicide()
{
	delete this;
}
float MusicWrapper::getDuration() const
{
	if(!m_player) throw std::runtime_error("Can't get duration - Music isn't initialized.");
	else return m_player->getDuration().asSeconds();
}
void MusicWrapper::play()
{
	if(!m_player) throw std::runtime_error("Can't play - Music isn't initialized.");
	else m_player->play();
}
void MusicWrapper::pause()
{
	if(!m_player) throw std::runtime_error("Can't pause - Music isn't initialized.");
	else m_player->pause();
}
void MusicWrapper::stop()
{
	if(!m_player) throw std::runtime_error("Can't stop - Music isn't initialized.");
	else m_player->stop();
}
unsigned int MusicWrapper::getChannelCount() const
{
	if(!m_player) throw std::runtime_error("Can't get channel count - Music isn't initialized.");
	else return m_player->getChannelCount();
}
unsigned int MusicWrapper::getSampleRate() const
{
	if(!m_player) throw std::runtime_error("Can't get sample rate - Music isn't initialized.");
	else return m_player->getSampleRate();
}
bool MusicWrapper::isPlaying() const
{
	if(!m_player) throw std::runtime_error("Can't get status - Music isn't initialized.");
	else return (m_player->getStatus() == sf::Music::Playing);
}
bool MusicWrapper::isPaused() const
{
	if(!m_player) throw std::runtime_error("Can't get status - Music isn't initialized.");
	else return (m_player->getStatus() == sf::Music::Paused);
}
bool MusicWrapper::isStopped() const
{
	if(!m_player) throw std::runtime_error("Can't get status - Music isn't initialized.");
	else return (m_player->getStatus() == sf::Music::Stopped);
}
void MusicWrapper::setPlayingOffset(float timeOffset)
{
	if(!m_player) throw std::runtime_error("Can't set playing offset - Music isn't initialized.");
	else m_player->setPlayingOffset(sf::seconds(timeOffset));
}
float MusicWrapper::getPlayingOffset() const
{
	if(!m_player) throw std::runtime_error("Can't get playing offset - Music isn't initialized.");
	else return m_player->getPlayingOffset().asSeconds();
}
void MusicWrapper::setLoop(bool loop)
{
	if(!m_player) throw std::runtime_error("Can't set loop - Music isn't initialized.");
	else m_player->setLoop(loop);
}
bool MusicWrapper::getLoop() const
{
	if(!m_player) throw std::runtime_error("Can't get loop - Music isn't initialized.");
	else return m_player->getLoop();
}
void MusicWrapper::setPitch(float pitch)
{
	if(!m_player) throw std::runtime_error("Can't set pitch - Music isn't initialized.");
	else m_player->setPitch(pitch);
}
void MusicWrapper::setVolume(float volume)
{
	if(!m_player) throw std::runtime_error("Can't set volume - Music isn't initialized.");
	else m_player->setVolume(volume);
}
void MusicWrapper::setPosition(float x, float y, float z)
{
	if(!m_player) throw std::runtime_error("Can't set position - Music isn't initialized.");
	else
	{
		this->x = x;
		this->y = y;
		this->z = z;
		UpdatePosition();
	}
}
void MusicWrapper::setX(float setto)
{
	if(!m_player) throw std::runtime_error("Can't set position - Music isn't initialized.");
	else
	{
		x = setto;
		UpdatePosition();
	}
}
void MusicWrapper::setY(float setto)
{
	if(!m_player) throw std::runtime_error("Can't set position - Music isn't initialized.");
	else
	{
		y = setto;
		UpdatePosition();
	}
}
void MusicWrapper::setZ(float setto)
{
	if(!m_player) throw std::runtime_error("Can't set position - Music isn't initialized.");
	else
	{
		z = setto;
		UpdatePosition();
	}
}
void MusicWrapper::setRelativeToListener(bool relative)
{
	if(!m_player) throw std::runtime_error("Can't set relation to listener - Music isn't initialized.");
	else m_player->setRelativeToListener(relative);
}
void MusicWrapper::setMinDistance(float distance)
{
	if(!m_player) throw std::runtime_error("Can't set minimal distance - Music isn't initialized.");
	else m_player->setMinDistance(distance);
}
void MusicWrapper::setAttenuation(float attenuation)
{
	if(!m_player) throw std::runtime_error("Can't set attenuation - Music isn't initialized.");
	else m_player->setAttenuation(attenuation);
}
float MusicWrapper::getPitch() const
{
	if(!m_player) throw std::runtime_error("Can't get pitch - Music isn't initialized.");
	else return m_player->getPitch();
}
float MusicWrapper::getVolume() const
{
	if(!m_player) throw std::runtime_error("Can't get volume - Music isn't initialized.");
	else return m_player->getVolume();
}
float MusicWrapper::getX() const
{
	return x;
}
float MusicWrapper::getY() const
{
	return y;
}
float MusicWrapper::getZ() const
{
	return z;
}
bool MusicWrapper::isRelativeToListener() const
{
	if(!m_player) throw std::runtime_error("Can't get relation to listener - Music isn't initialized.");
	else return m_player->isRelativeToListener();
}
float MusicWrapper::getMinDistance() const
{
	if(!m_player) throw std::runtime_error("Can't get minimal distance - Music isn't initialized.");
	else return m_player->getMinDistance();
}
float MusicWrapper::getAttenuation() const
{
	if(!m_player) throw std::runtime_error("Can't get attenuation- Music isn't initialized.");
	else return m_player->getAttenuation();
}
void MusicWrapper::setPosition(sf::Vector3f pos)
{
	if(!m_player) throw std::runtime_error("Can't get attenuation- Music isn't initialized.");
	else m_player->setPosition(pos);
}
sf::Vector3f MusicWrapper::getPosition() const
{
	if(!m_player) throw std::runtime_error("Can't get attenuation- Music isn't initialized.");
	else return m_player->getPosition();
}
