#ifndef MUSICWRAPPER_HPP
#define MUSICWRAPPER_HPP
#include "PhySFML.hpp"
#include <SFML/Audio/Music.hpp>

class MusicWrapper
{
private:
	PhySFML* m_stream;
	sf::Music* m_player;
	float x;
	float y;
	float z;
	void UpdatePosition();
	sf::Music* GetPlayer();
public:
	MusicWrapper(std::string filename);
	MusicWrapper(const char* filename);
	~MusicWrapper();
	void CommitSuicide();

	float getDuration() const;
	void play();
	void pause();
	void stop();
	unsigned int getChannelCount() const;
	unsigned int getSampleRate() const;
	bool isPlaying() const;
	bool isPaused() const;
	bool isStopped() const;
	void setPlayingOffset(float timeOffset);
	float getPlayingOffset() const;
	void setLoop(bool loop);
	bool getLoop() const;
	void setPitch(float pitch);
	void setVolume(float volume);
	void setPosition(float x, float y, float z);
	void setX(float setto);
	void setY(float setto);
	void setZ(float setto);
	void setRelativeToListener(bool relative);
	void setMinDistance(float distance);
	void setAttenuation(float attenuation);
	float getPitch() const;
	float getVolume() const;
	float getX() const;
	float getY() const;
	float getZ() const;
	bool isRelativeToListener() const;
	float getMinDistance() const;
	float getAttenuation() const;
	void setPosition(sf::Vector3f pos);
	sf::Vector3f getPosition() const;
};

#endif // MUSICWRAPPER_HPP
