#ifndef PHYSLUA_H
#define PHYSLUA_H
/*extern "C" {
#include <luajit.h>
}*/
#include "lua.hpp"
#include <string>
#include <vector>

typedef int (*lua_CFunction) (lua_State *L);
template<class T,const char* metatable> T* luaCheck(lua_State *L,int n)
{
	return *static_cast<T**>(luaL_checkudata(L,n,metatable) );
}
template<class T,const char* metatable> int lDestructor(lua_State *L)
{
	delete luaCheck<T,metatable>(L,1);
	return 0;
}

std::string StringizeBuffer(std::vector<char>& buf);

int lPHYSFS_loadFileString(lua_State *L);
int lPHYSFS_loadFileBuffer(lua_State *L);
//int lPHYSFS_doFile(lua_State *L);
int phys_DoFileL(lua_State *L,const char* filename); // legacy version
int phys_DoFile(lua_State *L,const char* filename);
int lua_getDirSeparator(lua_State *L);
int lua_enumerateFiles(lua_State *L);
int lua_enumerateFilesCallback(lua_State *L);
int lua_enumerateFullFiles(lua_State *L);
int lua_enumerateFullFilesCallback(lua_State *L);
int lua_ProcessFile(lua_State *L);
std::string ProcessFileForLua(lua_State *L, std::string parent_path, std::string file_name);
//int lua_FileProccessForTable(lua_State *L);
int lua_DirectoryToTable(lua_State *L);

int lua_exists(lua_State *L);
int lua_isDirectory(lua_State *L);
int lua_isSymbolicLink(lua_State *L);
int lua_getLastModTime(lua_State *L);
/*int lua_statR(lua_State *L);
int lua_statT(lua_State *L);*/

void RegisterLuaFunctions(lua_State *L);

#endif // PHYSLUA_H
