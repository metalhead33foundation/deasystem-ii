#ifndef PHYSFML_HPP
#define PHYSFML_HPP
#include <SFML/System/InputStream.hpp>
#include "Physfs4Cpp.hpp"

class PhySFML : public sf::InputStream, public PhysFs::FileHandle
{
public:
	PhySFML(std::string name);
	sf::Int64 read(void* data, sf::Int64 size);
	sf::Int64 seek(sf::Int64 position) ;
	sf::Int64 tell();
	sf::Int64 getSize();
};

#endif // PHYSFML_HPP
