#include "ImageWrapper.hpp"
#include <stdexcept>

ImageWrapper::ImageWrapper(std::string filename)
{
	m_handle = new PhySFML(filename);
	if(!m_handle) throw std::runtime_error("Unable to initialize the filehandle. Perhaps there is not enough memory?");
	m_img = new sf::Image();
	if(!m_img) throw std::runtime_error("Unable to initialize the file. Perhaps there is not enough memory?");
	m_img->loadFromStream(*m_handle);
}
ImageWrapper::~ImageWrapper()
{
	if(m_img) delete m_img;
	if(m_handle) delete m_handle;
}
sf::Vector2u ImageWrapper::getSize() const
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	return m_img->getSize();
}
void ImageWrapper::createMaskFromColor(const sf::Color &color, sf::Uint8 alpha)
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	m_img->createMaskFromColor(color,alpha);
}
void ImageWrapper::setPixel(unsigned int x, unsigned int y, const sf::Color &color)
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	m_img->setPixel(x,y,color);
}
sf::Color ImageWrapper::getPixel(unsigned int x, unsigned int y) const
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	return m_img->getPixel(x,y);
}
const sf::Uint8* ImageWrapper::getPixelsPtr() const
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	return m_img->getPixelsPtr();
}
const ImageWrapper::PixelContainer ImageWrapper::getPixelContainer() const
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	return ImageWrapper::PixelContainer(m_img->getPixelsPtr(),(m_img->getSize().x * m_img->getSize().y * 4));
}
void ImageWrapper::flipHorizontally()
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	m_img->flipHorizontally();
}
void ImageWrapper::flipVertically()
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	m_img->flipVertically();
}
sf::Texture* ImageWrapper::toTexture(sf::Texture* origin, const sf::IntRect& area)
{
	if(!m_img) throw std::runtime_error("ERROR! - Picture not initialized properly");
	if(!m_handle) throw std::runtime_error("ERROR - Picture not loaded properly!");
	sf::Texture* temp;
	if(origin) temp = origin;
	else temp = new sf::Texture();
	temp->loadFromImage(*m_img,area);
	return temp;
}
sf::Texture* ImageWrapper::loadTexture(std::string path)
{
	ImageWrapper temp_img(path);
	return temp_img.toTexture();
}
