#ifndef IMAGEWRAPPER_HPP
#define IMAGEWRAPPER_HPP
#include "PhySFML.hpp"
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Texture.hpp>

class ImageWrapper
{
private:
	PhySFML* m_handle;
	sf::Image* m_img;
public:
	struct PixelContainer {
		const sf::Uint8* ptr;
		const size_t size;
		PixelContainer(const sf::Uint8* n_ptr,size_t n_size)
			: ptr(n_ptr), size(n_size)
		{
			;
		}
	};
	sf::Vector2u getSize() const;
	void createMaskFromColor(const sf::Color &color, sf::Uint8 alpha=0);
	void setPixel(unsigned int x, unsigned int y, const sf::Color &color);
	sf::Color getPixel(unsigned int x, unsigned int y) const;
	const sf::Uint8* getPixelsPtr() const;
	const PixelContainer getPixelContainer() const;
	void flipHorizontally();
	void flipVertically();
	sf::Texture* toTexture(sf::Texture* origin=0,const sf::IntRect& area=sf::IntRect());
	static sf::Texture* loadTexture(std::string path);
	ImageWrapper(std::string filename);
	~ImageWrapper();
};

#endif // IMAGEWRAPPER_HPP
