#ifndef SOUNDBUFFERPHYSFS_HPP
#define SOUNDBUFFERPHYSFS_HPP
#include "PhySFML.hpp"
#include <SFML/Audio/SoundBuffer.hpp>

class SoundBufferPhysfs
{
private:
	PhySFML* m_handle;
	sf::SoundBuffer* m_buff;
public:
	SoundBufferPhysfs(std::string name);
	~SoundBufferPhysfs();
	const sf::Int16* getSamples() const;
	sf::Uint64 getSampleCount() const;
	unsigned int getSampleRate() const;
	unsigned int getChannelCount() const;
	sf::Time getDuration() const;
	const sf::SoundBuffer* GetBuffer() const;
};

#endif // SOUNDBUFFERPHYSFS_HPP
