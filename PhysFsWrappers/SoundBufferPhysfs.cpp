#include "SoundBufferPhysfs.hpp"

SoundBufferPhysfs::SoundBufferPhysfs(std::string name)
{
	m_handle = new PhySFML(name);
	if(!m_handle) throw std::runtime_error("Unable to initialize the filehandle. Perhaps there is not enough memory?");
	m_buff = new sf::SoundBuffer();
	if(!m_buff) throw std::runtime_error("Unable to initialize the file. Perhaps there is not enough memory?");
	m_buff->loadFromStream(*m_handle);
}
SoundBufferPhysfs::~SoundBufferPhysfs()
{
	if(m_buff) delete m_buff;
	if(m_handle) delete m_handle;
}
const sf::SoundBuffer* SoundBufferPhysfs::GetBuffer() const
{
	return m_buff;
}
const sf::Int16* SoundBufferPhysfs::getSamples() const
{
	if(!m_buff) throw std::runtime_error("Sound buffer not initialized yet.");
	else return m_buff->getSamples();
}
sf::Uint64 SoundBufferPhysfs::getSampleCount() const
{
	if(!m_buff) throw std::runtime_error("Sound buffer not initialized yet.");
	else return m_buff->getSampleCount();
}
unsigned int SoundBufferPhysfs::getSampleRate() const
{
	if(!m_buff) throw std::runtime_error("Sound buffer not initialized yet.");
	else return m_buff->getSampleRate();
}
unsigned int SoundBufferPhysfs::getChannelCount() const
{
	if(!m_buff) throw std::runtime_error("Sound buffer not initialized yet.");
	else return m_buff->getChannelCount();
}
sf::Time SoundBufferPhysfs::getDuration() const
{
	if(!m_buff) throw std::runtime_error("Sound buffer not initialized yet.");
	else return m_buff->getDuration();
}
