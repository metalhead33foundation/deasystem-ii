#include "PhysLua.hpp"
#include "Physfs4Cpp.hpp"
extern "C" {
#include <stdlib.h>
#include <jemalloc/jemalloc.h>
}

std::string StringizeBuffer(std::vector<char>& buf)
{
	return std::string(&buf[0],buf.size());
}

int lPHYSFS_loadFileString(lua_State *L)
{
	int n = lua_gettop(L);
	if(!n)
	{
		lua_pushnil(L);
		return 1;
	}

	const char* name = lua_tostring(L,1);
	PHYSFS_File* fail = PHYSFS_openRead(name);
	if(!fail)
	{
		return luaL_error(L,PHYSFS_getLastError());
	}
	size_t len = PHYSFS_fileLength(fail);
	void* buf;
	buf = malloc(len);
	PHYSFS_read(fail,buf,1,len);
	PHYSFS_close(fail);
	lua_pushlstring(L,(const char*)buf,len);
	free(buf);
	return 1;
}
int lPHYSFS_loadFileBuffer(lua_State *L)
{
	int n = lua_gettop(L);
	if(!n)
	{
		lua_pushnil(L);
		return 1;
	}

	const char* name = lua_tostring(L,1);
	PHYSFS_File* fail = PHYSFS_openRead(name);
	if(!fail)
	{
		return luaL_error(L,PHYSFS_getLastError());
	}
	size_t len = PHYSFS_fileLength(fail);
	void* buf;
	buf = malloc(len);
	PHYSFS_read(fail,buf,1,len);
	PHYSFS_close(fail);
	luaL_loadbuffer(L,(const char*)buf,len,name);
	free(buf);
	return 1;
}
int phys_DoFile(lua_State *L,const char* filename)
{
	int to_return = 0;
	PHYSFS_File* fail = PHYSFS_openRead(filename);
	if(!fail)
	{
		return 0;
	}
	size_t len = PHYSFS_fileLength(fail);
	void* buf;
	buf = malloc(len);
	PHYSFS_read(fail,buf,1,len);
	PHYSFS_close(fail);
	luaL_loadstring(L, (const char*)buf);
	free(buf);
	to_return = lua_pcall(L, 0, LUA_MULTRET, 0);
	return to_return;
}
int phys_DoFileL(lua_State *L,const char* filename)
{
	int to_return = 0;
	PHYSFS_File* fail = PHYSFS_openRead(filename);
	if(!fail)
	{
		return 0;
	}
	size_t len = PHYSFS_fileLength(fail);
	void* buf;
	buf = malloc(len);
	PHYSFS_read(fail,buf,1,len);
	PHYSFS_close(fail);
	luaL_loadbuffer(L,(const char*)buf,len,filename);
	free(buf);
	to_return = lua_pcall(L, 0, LUA_MULTRET, 0);
	return to_return;
}
int lua_getDirSeparator(lua_State *L)
{
lua_pushstring(L,PHYSFS_getDirSeparator());
return 1;
}
int lua_enumerateFiles(lua_State *L)
{
	char **rc = PHYSFS_enumerateFiles(luaL_checkstring(L,1));
	char **i;
	int it;
	//lua_pushstring(L,PHYSFS_getDirSeparator());
	lua_newtable(L);
	/*
	 * Stack:
	 * -1 = new table
	 * -2 = path string
	*/
	for (i = rc, it = 1; *i != NULL; i++,++it)
	{
		lua_pushstring(L,*i);
		/*
		 * Stack:
		 * -1 = new string
		 * -2 = table
		 * -3 = path string
		*/
		lua_rawseti(L, -2, it);
		/*
		 * Stack:
		 * -1 = new table
		 * -2 = path string
		*/
	}
	PHYSFS_freeList(rc);
	lua_pushinteger(L,it);
	return 2;
}
int lua_enumerateFilesCallback(lua_State *L)
{
	char **rc = PHYSFS_enumerateFiles(luaL_checkstring(L,1));
	int top = lua_gettop(L);
	char **i;
	int it;
	if(!lua_isfunction(L,2)) return luaL_error(L,"The second argument of \"enumerateFilesCallback\" should always be a function!");
	for (i = rc, it = 1; *i != NULL; i++,++it)
	{
		lua_pushvalue (L, 2);
		lua_pushstring(L,*i);
		lua_pushinteger(L,it);
		lua_call(L, 2, 0);
	}
	lua_settop(L,top);
	PHYSFS_freeList(rc);
	lua_pushinteger(L,it);
	return 1;
}


int lua_enumerateFullFiles(lua_State *L)
{
	std::string path = std::string(luaL_checkstring(L,1) );
	if(path.back() != *PHYSFS_getDirSeparator() ) path += PHYSFS_getDirSeparator();
	char **rc = PHYSFS_enumerateFiles(path.c_str() );
	char **i;
	int it;
	//lua_pushstring(L,PHYSFS_getDirSeparator());
	lua_newtable(L);
	/*
	 * Stack:
	 * -1 = new table
	 * -2 = path string
	*/
	for (i = rc, it = 1; *i != NULL; i++,++it)
	{
		std::string tmp = path;
		tmp += *i;
		lua_pushstring(L,tmp.c_str());
		/*
		 * Stack:
		 * -1 = new string
		 * -2 = table
		 * -3 = path string
		*/
		lua_rawseti(L, -2, it);
		/*
		 * Stack:
		 * -1 = new table
		 * -2 = path string
		*/
	}
	PHYSFS_freeList(rc);
	lua_pushinteger(L,it);
	return 2;
}
int lua_enumerateFullFilesCallback(lua_State *L)
{
	std::string path = std::string(luaL_checkstring(L,1) );
	if(path.back() != *PHYSFS_getDirSeparator() ) path += PHYSFS_getDirSeparator();
	char **rc = PHYSFS_enumerateFiles(path.c_str() );
	int top = lua_gettop(L);
	char **i;
	int it;
	if(!lua_isfunction(L,2)) return luaL_error(L,"The second argument of \"enumerateFilesCallback\" should always be a function!");
	for (i = rc, it = 1; *i != NULL; i++,++it)
	{
		std::string tmp = path;
		tmp += *i;
		lua_pushvalue (L, 2);
		lua_pushstring(L,tmp.c_str());
		lua_pushinteger(L,it);
		lua_call(L, 2, 0);
	}
	lua_settop(L,top);
	PHYSFS_freeList(rc);
	lua_pushinteger(L,it);
	return 1;
}

int lua_exists(lua_State *L)
{
	lua_pushboolean(L,PHYSFS_exists(luaL_checkstring(L,1)));
	return 1;
}
int lua_isDirectory(lua_State *L)
{
	lua_pushboolean(L,PHYSFS_isDirectory(luaL_checkstring(L,1)));
	return 1;
}
int lua_isSymbolicLink(lua_State *L)
{
	lua_pushboolean(L,PHYSFS_isSymbolicLink(luaL_checkstring(L,1)));
	return 1;
}
int lua_getLastModTime(lua_State *L)
{
	lua_pushinteger(L,PHYSFS_getLastModTime(luaL_checkstring(L,1)));
	return 1;
}
/*int lua_statR(lua_State *L)
{
	PHYSFS_Stat stat;
	//lua_pushinteger(L,PHYSFS_getLastModTime(luaL_checkstring(L,1)));
	return 1;
}
int lua_statT(lua_State *L);*/
int lua_ProcessFile(lua_State *L)
{
	try {
	std::string fullpath = std::string(luaL_checkstring(L,1));
	std::size_t found = fullpath.find_last_of(".");
	std::string name = fullpath.substr(0,found);
	std::string extension = fullpath.substr(found+1);
	PhysFs::FileHandle file(fullpath,PhysFs::FileHandle::READ);
	//std::string fileself = file.stringize();
	PhysFs::FileHandle::ByteArray buffer = file.loadIntoMemory();
	if(extension == "int")
	{
		lua_pushinteger(L,std::stol(StringizeBuffer(buffer) ));
	}
	else if(extension == "float")
	{
		lua_pushnumber(L,std::stod(StringizeBuffer(buffer) ));
	}
	else if(extension == "bool")
	{
		if(StringizeBuffer(buffer) == "true") lua_pushboolean(L,true);
		else lua_pushboolean(L, false);
	}
	else if(extension == "func")
	{
		//luaL_loadstring(L,fileself.c_str() );
		luaL_loadbuffer(L,&buffer[0],buffer.size(),name.c_str() );
		lua_call(L,0,1);
	}
	else lua_pushlstring(L,&buffer[0],buffer.size() );
	return 1;
	} catch(std::exception e)
	{
		return luaL_error(L,e.what() );
	}
}
std::string ProcessFileForLua(lua_State *L, std::string parent_path, std::string file_name)
{
	std::string fullpath = parent_path;
	if(fullpath.back() != *PHYSFS_getDirSeparator() ) fullpath += PHYSFS_getDirSeparator();
	fullpath+= file_name;
	std::size_t found = file_name.find_last_of(".");
	//std::string name = fullpath.substr(0,found);
	std::string to_return = file_name.substr(0,found);
	std::string extension = file_name.substr(found+1);
	try {
	PhysFs::FileHandle file(fullpath,PhysFs::FileHandle::READ);
	std::string fileself = file.stringize();
	if(PHYSFS_isDirectory(fullpath.c_str() ) )
	{
		std::string path = fullpath;
		if(path.back() != *PHYSFS_getDirSeparator() ) path += PHYSFS_getDirSeparator();
		lua_newtable(L);
		char **rc = PHYSFS_enumerateFiles(path.c_str() );
		char **i;
		for (i = rc; *i != NULL; i++)
		{
			/* STACK
			 * Before ProcessFileForLua:
			 * -1 = table
			*/
			std::string tmp = ProcessFileForLua(L,path,*i);
			/*
			 * After ProcessFileForLua:
			 * -1 = whatever the file was
			 * -2 = table
			*/
			//lua_setfield(L,-2,tmp.c_str());
			/* STACK
			 * After lua_setfield:
			 * -1 = table
			*/
		}
		PHYSFS_freeList(rc);
	}
	else if(extension == "int")
	{
		lua_pushinteger(L,std::stol(fileself));
	}
	else if(extension == "float")
	{
		lua_pushnumber(L,std::stod(fileself));
	}
	else if(extension == "bool")
	{
		if(fileself == "true") lua_pushboolean(L,true);
		else lua_pushboolean(L, false);
	}
	else if(extension == "func")
	{
		luaL_loadstring(L,fileself.c_str() );
		lua_call(L,0,1);
	}
	else lua_pushstring(L,fileself.c_str());
	} catch(std::exception e)
	{
		lua_pushnil(L);
	}
	lua_setfield(L,-2,to_return.c_str() );
	return to_return;
}

/*int lua_FileProccessForTable(lua_State *L)
{
	std::string fullpath = std::string(luaL_checkstring(L,1));
	if( !lua_istable(L,2) ) return luaL_error(L,"Expected a table!");
	lua_setfield(L,2,ProcessFileForLua(L,fullpath).c_str() );
	return 0;
}*/
int lua_DirectoryToTable(lua_State *L)
{
	std::string fullpath = std::string(luaL_checkstring(L,-1));
	if(fullpath.back() != *PHYSFS_getDirSeparator() ) fullpath += PHYSFS_getDirSeparator();
	/*
	 * Stack:
	 * -1 = string
	*/
	lua_newtable(L);
	/*
	 * Stack:
	 * -1 = table
	 * -2 = string
	*/
	char **rc = PHYSFS_enumerateFiles(fullpath.c_str() );
	char **i;
	for (i = rc; *i != NULL; i++)
	{
		ProcessFileForLua(L,fullpath,*i);
	}
	PHYSFS_freeList(rc);
	return 1;
}

const luaL_Reg PhysfsFunctions[] = {
	{ "loadFileString)", lPHYSFS_loadFileString },
	{ "loadFileBuffer", lPHYSFS_loadFileBuffer },
	{ "getDirSeparator", lua_getDirSeparator },
	{ "enumerateFiles", lua_enumerateFiles },
	{ "enumerateFilesCallback", lua_enumerateFilesCallback },
	{ "enumerateFullPath", lua_enumerateFullFiles },
	{ "enumerateFullPathCallback", lua_enumerateFullFilesCallback },
	{ "exists", lua_exists },
	{ "isDirectory", lua_isDirectory },
	{ "isSymbolicLink", lua_isSymbolicLink },
	{ "getLastModTime", lua_getLastModTime },
	{ "processFile", lua_ProcessFile },
	// { "fileToField", lua_FileProccessForTable },
	//{ "processFileForTable", lua_FileProccessForTable },
	{ "directoryToTable", lua_DirectoryToTable },
	//{ "statR", lua_statR },
	//{ "statT", lua_statT },
	{ NULL, NULL }
};

const char* LoadFileFunc = "function physfs.dofile(filename)\n"
		"\tlocal f = assert(physfs.loadFileBuffer(filename))\n"
		"\treturn f()\n"
		"end\n\0";

void RegisterLuaFunctions(lua_State *L)
{
	/*lua_createtable(L,0,1);
	lua_setfield(L, LUA_GLOBALSINDEX, "physfs");
	lua_getfield(L, LUA_GLOBALSINDEX, "physfs");*/
	luaL_register(L, "physfs", PhysfsFunctions);
	luaL_dostring(L,LoadFileFunc);
	//lua_register(L, "PHYSFS_DoFile",lPHYSFS_doFile);
}
